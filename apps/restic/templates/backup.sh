#!/usr/bin/env bash
# reasonable bash options
set -o errexit
set -o nounset
set -o pipefail

# set b2 configuration file
CONF_FILE="{{ restic_conf.conf_path }}"

## Storage

# backup the specified folders inside the DATA_DIR
DATA_DIR="{{ storage_conf.storage_base_dir }}/{{ storage_conf.data_subdir }}"

# given the mounting setup, each directory will have /home/$FOLDER as
# path of the restic snapshot
declare -a FOLDERS=(
    {% for name in storage_conf.folders_to_backup %}
    "{{ name }}"
    {% endfor %}
)

for folder in "${FOLDERS[@]}"
do
    docker run \
           --env-file "$CONF_FILE" \
           --volume "$DATA_DIR:/home/" \
           restic/restic:latest \
           backup "/home/$folder/" \
           --tag "$folder" \
           --host "{{ restic_conf.host_label }}" \
           --verbose

	# apply the forget policy on the folder snapshots

	# please note that when forgetting some tag snapshots, they are
	# automatically grouped per host: this is why it was set
	# explicitely in the previous command

	# the policy here is to keep the single most recent snapshot of
	# - each of the last seven day
	# - each of the last 5 weeks
	# - each of the last 12 months
	# - each of the last 100 years (one per year)
	docker run \
           --env-file "$CONF_FILE" \
           restic/restic:latest \
           forget \
           --tag "$folder" \
		   --keep-daily 7 \
		   --keep-weekly 5 \
		   --keep-monthly 12 \
		   --keep-yearly 100 \
           --verbose
done

## Apps

# backup miniflux database, saving it in a temporary directory
MINIFLUX_TEMP=/tmp/miniflux-database-backup
mkdir -p "$MINIFLUX_TEMP"

MINIFLUX_COMPOSE="{{ miniflux_conf.base_dir }}/docker-compose.yml"
docker-compose --file "$MINIFLUX_COMPOSE" exec -T db \
               pg_dump --host localhost -U miniflux -d miniflux \
               > "$MINIFLUX_TEMP/miniflux.sql"

# store the backup in a snapshot with path /apps/miniflux/
docker run \
       --env-file "$CONF_FILE" \
       --volume "$MINIFLUX_TEMP:/apps/miniflux/" \
       restic/restic:latest \
       backup /apps/miniflux/ \
       --tag "miniflux" \
       --host "{{ restic_conf.host_label }}" \
       --verbose

# please note that when forgetting some tag snapshots, they are
# automatically grouped per host: this is why it was set
# explicitely in the previous command

# the policy here is to keep only
# - the most recent snapshot, for easy restoring of the current status
# - the single most recent for each of the last 2 months, in the
#   unlucky case miniflux corrupts the database
docker run \
       --env-file "$CONF_FILE" \
       restic/restic:latest \
       forget \
       --tag "miniflux" \
	   --keep-last 1 \
	   --keep-monthly 2 \
       --verbose

# cleanup miniflux directory
rm "$MINIFLUX_TEMP/miniflux.sql"
rmdir "$MINIFLUX_TEMP"

## Prune forgot snapshot data, checking later the status of the repo
docker run \
       --env-file "$CONF_FILE" \
       restic/restic:latest \
	   prune \
       --verbose

docker run \
       --env-file "$CONF_FILE" \
       restic/restic:latest \
	   check \
       --verbose
